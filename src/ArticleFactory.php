<?php

class ArticleFactory
{
    /**
     * @var MySQLDatabase
     */
    private $db;

    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    /**
     * @param $id
     *
     * @return Article
     */
    public function getById($id)
    {
        # $this->db->query(...)

        return new Article($id);
    }
}