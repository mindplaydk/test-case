<?php

interface Database
{
    public function query($sql);
}