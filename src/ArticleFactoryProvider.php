<?php

interface ArticleFactoryProvider
{
    /**
     * @return ArticleFactory
     */
    public function create();
}
