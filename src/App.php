<?php

use Pimple\Container;

class App
{
    /**
     * @var Container
     */
    public $container;

    public function __construct()
    {
        $container = new Container();

        $container['Database'] = function ($c) {
            return new MySQLDatabase();
        };

        $container['UserFactory'] = $container->factory(function ($c) {
            return new UserFactory($c['Database']);
        });

        $container['UserFactoryProvider'] = function ($c) {
            return new UserFactoryProviderImpl($c);
        };

        $container['UserService'] = function ($c) {
            return new UserService($c['UserFactoryProvider']);
        };

        $container['ArticleFactory'] = $container->factory(function ($c) {
            return new ArticleFactory($c['Database']);
        });

        $container['ArticleFactoryProvider'] = function ($c) {
            return new ArticleFactoryProviderImpl($c);
        };

        $container['ArticleService'] = function ($c) {
            return new ArticleService($c['ArticleFactoryProvider']);
        };

        $container['AuthenticationService'] = function ($c) {
            return new AuthenticationService(
                $c['ArticleService'],
                $c['UserService']
            );
        };

        $this->container = $container;
    }
}
