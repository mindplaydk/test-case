<?php

interface UserFactoryProvider
{
    /**
     * @return UserFactory
     */
    public function create();
}
