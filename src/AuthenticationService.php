<?php

class AuthenticationService
{
    /**
     * @var int[]
     */
    private $allowed_users = array();

    /**
     * @var UserService
     */
    private $users;

    /**
     * @var ArticleService
     */
    private $articles;

    /**
     * @param ArticleService $articles
     * @param UserService    $users
     */
    public function __construct(ArticleService $articles, UserService $users)
    {
        $this->users = $users;
        $this->articles = $articles;
    }

    /**
     * @param int $user_id
     *
     * @return void
     */
    public function allowRead($user_id)
    {
        $this->allowed_users[] = $user_id;
    }

    /**
     * @param int $user_id
     * @param int $article_id
     *
     * @return bool
     */
    public function canRead($user_id, $article_id)
    {
        return in_array($user_id, $article_id);
    }
}
