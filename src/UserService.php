<?php

class UserService
{
    /**
     * @var UserFactoryProvider
     */
    private $provider;

    /**
     * @param UserFactoryProvider $provider
     */
    public function __construct(UserFactoryProvider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param int $id
     *
     * @return User
     */
    public function getById($id)
    {
        $f = $this->provider->create();

        return $f->getById($id);
    }
}
