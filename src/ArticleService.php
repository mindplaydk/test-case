<?php

class ArticleService
{
    /**
     * @var ArticleFactoryProvider
     */
    private $provider;

    /**
     * @param ArticleFactoryProvider $provider
     */
    public function __construct(ArticleFactoryProvider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param int $id
     *
     * @return Article
     */
    public function getById($id)
    {
        $f = $this->provider->create();

        return $f->getById($id);
    }
}
