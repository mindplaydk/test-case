<?php

class UserFactory
{
    /**
     * @var MySQLDatabase
     */
    private $db;

    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    /**
     * @param $id
     *
     * @return User
     */
    public function getById($id)
    {
        $this->db->query('SELECT...');

        return new User($id);
    }
}