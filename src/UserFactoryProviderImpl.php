<?php

use Pimple\Container;

class UserFactoryProviderImpl implements UserFactoryProvider
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        return $this->container['UserFactory'];
    }
}
