<?php

require __DIR__ . '/header.php';

$app = new App();

// example: can override built-in factory function:
$app->container['ArticleFactoryProvider'] = function ($c) {
    echo "\n*** HELLO from replaced ArticleFactoryProvider factory function!\n\n";
    return new ArticleFactoryProviderImpl($c);
};

$app->container['Database']->query("SELECT...");

var_dump($app->container['UserService']->getById(123));

var_dump($app->container['ArticleService']->getById(456));
